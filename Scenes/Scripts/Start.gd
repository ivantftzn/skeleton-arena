extends LinkButton

export(String) var scene_to_load

func _on_Start_pressed():
	Global.player_hp = 200
	get_tree().change_scene(str("res://Scenes/Level 1.tscn"))

func _on_Exit_pressed():
	get_tree().quit()


func _on_Main_Menu_pressed():
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
