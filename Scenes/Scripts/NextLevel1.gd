extends Area2D

func _on_NextLevel1_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/Level 2.tscn"))

func _on_NextLevel2_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/WinScreen.tscn"))

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
