extends KinematicBody2D
export var hp = 5
export (int) var GRAVITY = 1200
const UP = Vector2(0,-1)
var velocity = Vector2()

export var movespeed = 200
var moving_right = true
var is_attacking = false
var can_move = true

func _ready():
	pass # Replace with function body.

func dead():
	set_collision_layer_bit(2, false)
	set_collision_mask_bit(1, false)
	$AnimatedSprite.play("death")
	is_attacking = true
	can_move = false
	$AttackColl/CollisionShape2D.disabled = true
	$PlayerDetection/PlayerDetector.disabled = true
	
func take_damage():
	if hp > 0:
		hp -= 1
		if hp == 0:
			dead()
		elif not is_attacking:
			$AnimatedSprite.play("take_damage")
			can_move = false

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "death":
		$AnimatedSprite.play("dead")
	if $AnimatedSprite.animation == "take_damage":
		$AnimatedSprite.play("idle")
		can_move = true
	if $AnimatedSprite.animation == "attack":
		$AttackColl/CollisionShape2D.disabled = true
		$PlayerDetection/PlayerDetector.disabled = true
		is_attacking = false
		$AnimatedSprite.play("idle")
		$StopTimer.set_wait_time(1)
		$StopTimer.start()

func _physics_process(delta):	
	if can_move:
		velocity.y += delta * GRAVITY
		
		if moving_right and not is_attacking:
			velocity.x = movespeed
			$AnimatedSprite.play("walk")
		elif not is_attacking:
			velocity.x = -movespeed
			
		velocity = move_and_slide(velocity, UP)
		
		if $CollisionDetect.is_colliding():
			moving_right = !moving_right
			scale.x = -scale.x
	else:
		velocity.x = 0
		
func _on_SkeletonHitbox_area_entered(area):
	if area.is_in_group("Attack"):
		take_damage()


func _on_PlayerDetection_body_entered(body):
	if body.get_name() == "Player" and not is_attacking:
		is_attacking = true
		can_move = false
		velocity.x = 0
		$AnimatedSprite.play("attack")


func _on_AnimatedSprite_frame_changed():
	if $AnimatedSprite.animation == "attack" and $AnimatedSprite.frame == 5:
		$AttackColl/CollisionShape2D.disabled = false


func _on_StopTimer_timeout():
	can_move = true
	$PlayerDetection/PlayerDetector.disabled = false
	$StopTimer.stop()
