extends KinematicBody2D

export (int) var speed = 300
export (int) var jump_speed = -400
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var animation = ""
var is_attacking = false
var double_jumped = false
var is_dead = false
var is_knocked = false

func dead():
	is_dead = true
	animation = "death"
	
func take_damage():
	if Global.player_hp > 0:
		Global.player_hp -= 1
		if Global.player_hp <= 0:
			dead()
			
func take_hit():
	if Global.player_hp > 0:
		Global.player_hp -= 30
		if Global.player_hp <= 0:
			dead()
		else:
			is_knocked = true
			$InvincibilityTimer.set_wait_time(1)
			$InvincibilityTimer.start()
			animation = "knocked"

func get_input():	
	velocity.x = 0
	if not is_dead:
		if Input.is_action_pressed('ui_right') and not is_knocked:
			$AnimatedSprite.flip_h = false
			$AttackColl.position.x = 0
			if is_on_floor() and not is_attacking:
				animation = "walk"
			velocity.x += speed
		if Input.is_action_pressed('ui_left') and not is_knocked:
			$AnimatedSprite.flip_h = true
			$AttackColl.position.x = -52.5
			if is_on_floor() and not is_attacking:
				animation = "walk"
			velocity.x -= speed
		if is_on_floor():
			if not Input.is_action_pressed('ui_right') and not Input.is_action_pressed('ui_left') and not is_attacking and not is_knocked:
				animation = "idle"
			double_jumped = false
			if Input.is_action_just_pressed('ui_up') and not is_knocked:
				velocity.y = jump_speed
		if not is_on_floor() and not is_attacking and not is_knocked:
			animation = "jump"
		if not double_jumped and Input.is_action_just_pressed('ui_up') and not is_knocked:
			velocity.y = jump_speed
			double_jumped = true
		if Input.is_action_just_pressed('ui_select') and not is_knocked:
			animation = "attack"
			is_attacking = true
			
		
func _process(delta):
	$AnimatedSprite.play(animation)

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if "Skeleton" in collision.collider.name:
			take_damage()


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "attack":
		is_attacking = false
	$AttackColl/CollisionShape2D.disabled = true
	if $AnimatedSprite.animation == "death":
		animation = "dead"
	if $AnimatedSprite.animation == "knocked":
		is_knocked = false
	if $AnimatedSprite.animation == "death":
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))

func _on_AnimatedSprite_frame_changed():
	if $AnimatedSprite.animation == "attack" and $AnimatedSprite.frame == 2:
		$AttackColl/CollisionShape2D.disabled = false

func _on_PlayerHitbox_area_entered(area):
	if area.is_in_group("EnemyAttack") and $InvincibilityTimer.time_left==0:
		take_hit()
	if area.is_in_group("Death"):
		dead()

func _on_Timer_timeout():
	$InvincibilityTimer.stop()
